import java.util.Scanner;

public class main {

	public static void main(String[] args) {

		System.out.println("Choose a colour: Black, White, Red, Blue ");

		Scanner user_input = new Scanner(System.in);

		String color = user_input.next();


		switch ( color ) {

			case "Black":
				System.out.println("You selected the Black color");
				break;
			case "White":
				System.out.println("You selected the White color");
				break;
			case "Red":
				System.out.println("You selected the Red color");
				break;
			case "Blue":
				System.out.println("You selected the Blue color");
				break;
	
			default:
	
				System.out.println("color" +  color +  " has not been found.");

		}
	}	


}

